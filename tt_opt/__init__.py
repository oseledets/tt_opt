try:
    import numpy as np
    import maxvol
except:
    import ctypes
    try:
        ctypes.CDLL("libmkl_rt.so", ctypes.RTLD_GLOBAL)
    except:
        try:
            ctypes.CDLL("liblapack.so", ctypes.RTLD_GLOBAL)
        except:
            print "Did not find MKL or LAPACK library"

from tt_min import *
