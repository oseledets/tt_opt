from tt_opt import tt_min
from scipy.optimize import rosen
#fun = lambda x, y: x ** 2 + y ** 2
#tt_min(fun, -2, 2, d = 2)
#fun = lambda x : x[:, 0] ** 2 + x[:, 1] ** 2 + x[:, 2] ** 2
def my_rosen(x):
    return rosen(x.T)
val, x_full = tt_min(my_rosen, -2, 2, d = 4, n0 = 512, rmax = 10, nswp = 30)
