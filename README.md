tt-opt
====
Empirical global minimization methods via tensor train sampling
This work was supported by the Grant of the Yandex company ``Tensor and matrix methods for optimization and latent variable models''.


Installation
============

##Downloading the code
```
git clone --recursive git://github.com/oseledets/ttpy.git

```


##Installing the package
The preferred way to install the package is by running
```
python setup.py build_ext --inplace
```

##BLAS and so on
We try to do **dynamic loading** of BLAS/LAPACK libraries, so you can encounter "undefined symbols" error 
during import of **tt_opt** package. 


- Use the Enthought Python Distribution (EPD) (non-Free version, but it is free for academics).
  It loads the well-tuned MKL library into the global namespace, and you do not have to do anything.
  Also, you can use non-free Anaconda Continuum Python Distribution, that does the same trick. Everything should work "as it is".

- Have    ``libmkl_rt.so`` or ``liblapack.so`` somewhere on you ``LD_LIBRARY_PATH``. In this case, everything will work as well.










