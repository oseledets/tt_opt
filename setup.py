#This script will build the main subpackages  
from distutils.util import get_platform 
from numpy.distutils.misc_util import Configuration, get_info
import sys

def configuration(parent_package='',top_path=None):
    sys.argv.extend ( ['config_fc', '--fcompiler=gnu95'])
    config = Configuration(None, parent_package, top_path) 
    config.set_options(ignore_setup_xxx_py=True,
                       assume_default_configuration=True,
                       delegate_options_to_subpackages=True,
                       quiet=False,     
    )
    
    config.add_subpackage('tt_opt')
    return config
    


if __name__ == '__main__':
    from numpy.distutils.core import setup
    setup(**configuration(top_path='').todict())
